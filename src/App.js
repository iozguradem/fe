import React, { useState } from "react";
import styled from "styled-components";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./styles.css";
import { ListPage, AddPage } from "./pages";
import Toast from "./components/Toast";
import Header from "./components/Header";

const PageContainer = styled.div`
  max-width: 1200px;
  margin: auto;
`;

const Content = styled.div`
  margin: auto;
  padding: 20px 10px;
  max-width: 400px;
`;

function App() {
  const [toast, setToast] = useState(null);

  return (
    <Router>
      <>
        <Switch>
          <PageContainer>
            <Header />
            <Content>
              <Route path="/add">
                <AddPage setToast={setToast} />
              </Route>
              <Route path="/" exact>
                <ListPage setToast={setToast} />
              </Route>
            </Content>
          </PageContainer>
        </Switch>
        {toast && <Toast message={toast} handleClose={() => setToast(null)} />}
      </>
    </Router>
  );
}

export default App;
