import React from "react";
import { configure, mount } from "enzyme";
import { MemoryRouter } from "react-router";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";
import App from "./App";

configure({ adapter: new Adapter() });

const items = [
  {
    id: 1,
    name: "Hepsiburada",
    url: "https://hepsiburada.com",
    vote: 0,
  },
];

const setItems = (newState) => {
  items.splice(0, items.length);
  items.push(...newState);
};

jest.mock("./hooks/useItems", function () {
  return () => {
    return [items, setItems];
  };
});

describe("<App />", () => {
  test("should be able to show the link list page", () => {
    const wrapper = mount(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    expect(wrapper.text()).toContain("Hepsiburada");
    expect(wrapper.text()).toContain("Up Vote");
    expect(wrapper.text()).toContain("Less Voted");
  });

  test("should be able to change the insert form", () => {
    render(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );

    screen.getByText("Hepsiburada");

    const insertLink = screen.getByTestId("form-link");
    user.click(insertLink);
    screen.getByText("Link Name:");

    const returnLink = screen.getByTestId("return-to-link-link");
    user.click(returnLink);
    screen.getByText("Hepsiburada");
  });
});
