import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import TextInput from "./index";

configure({ adapter: new Adapter() });

describe("<TextInput />", () => {
  test("should be able show the label", () => {
    const wrapper = shallow(<TextInput label="Name" />);
    expect(wrapper.text()).toBe("Name");
  });

  test("should has the label and the input elements", () => {
    const wrapper = mount(<TextInput label="Name" />);
    expect(wrapper.html()).toContain("<label");
    expect(wrapper.html()).toContain("<input");
  });

  test("should be able to pass additional props to the input", () => {
    const handleChange = jest.fn();
    const wrapper = mount(<TextInput label="Name" onChange={handleChange} />);

    const [input] = wrapper.find("input");
    input.props.onChange();

    expect(handleChange.mock.calls.length).toBe(1);
  });
});
