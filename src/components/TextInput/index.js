import React from "react";
import styled from "styled-components";

const Container = styled.div`
  margin-bottom: 30px;
  display: block;
  box-sizing: border-box;
`;

const Label = styled.label`
  display: block;
  margin-bottom: 4px;
`;

const StyledTextInput = styled.input`
  font-size: 16px;
  padding: 12px;
  border: 1px solid #ddd;
  border-radius: 4px;
  width: 100%;
  box-sizing: border-box;

  &:focus {
    outline: none;
  }

  &::placeholder {
    color: #ddd;
  }
`;

export default function TextInput({ label, ...props }) {
  return (
    <Container>
      <Label>{label}</Label>
      <StyledTextInput {...props} />
    </Container>
  );
}
