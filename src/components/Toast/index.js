import React, { useEffect } from "react";
import styled from "styled-components";

const Container = styled.div`
  position: absolute;
  top: 40px;
  left: 0px;
  right: 0px;
  margin: auto;
  width: 400px;
  background-color: #ddf9d3;
  border: 1px solid #3ca420;
  color: #3ca420;
  padding: 20px 30px;
  border-radius: 4px;
  font-size: 26px;
  text-align: center;
  box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.75);
  -webkit-box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.75);
  -moz-box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.75);
  cursor: pointer;
`;

export default function Toast({ message, handleClose, defaultTimeout = 3000 }) {
  useEffect(() => {
    const cleaner = setTimeout(() => {
      clearTimeout(cleaner);
      handleClose();
    }, defaultTimeout);
  }, []);

  return (
    <Container
      onClick={handleClose}
      dangerouslySetInnerHTML={{ __html: message }}
    ></Container>
  );
}
