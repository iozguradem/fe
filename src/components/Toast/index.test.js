import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Toast from "./index";

configure({ adapter: new Adapter() });

describe("<Toast />", () => {
  test("should be able show the message", () => {
    const wrapper = mount(<Toast message="This is the mesage" />);
    expect(wrapper.text()).toBe("This is the mesage");
  });

  test("should be able show an HTML content", () => {
    const wrapper = mount(<Toast message="This is the <b>mesage</b>" />);
    expect(wrapper.html()).toContain("This is the <b>mesage</b>");
  });

  test("should be able close automatically", async () => {
    const handleClose = jest.fn();

    mount(
      <Toast
        message="This is the message"
        defaultTimeout="100"
        handleClose={handleClose}
      />
    );
    await new Promise((resolve) => setTimeout(resolve, 200));

    expect(handleClose.mock.calls.length).toBe(1);
  });

  test("should be able close by clicking the toast message", async () => {
    const handleClose = jest.fn();

    const wrapper = mount(
      <Toast message="This is the message" handleClose={handleClose} />
    );

    const [container] = wrapper.find("div");
    container.props.onClick();

    expect(handleClose.mock.calls.length).toBe(1);
  });
});
