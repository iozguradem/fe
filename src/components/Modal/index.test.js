import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Modal from "./index";

configure({ adapter: new Adapter() });

describe("<Modal />", () => {
  test("should be able display the title and the content", () => {
    const wrapper = shallow(<Modal title="My Modal">Content</Modal>);
    expect(wrapper.text()).toContain("My Modal");
    expect(wrapper.text()).toContain("Content");
  });

  test("should have the close button", () => {
    const handler = jest.fn();
    const wrapper = mount(
      <Modal title="My Modal" handleCancel={handler}>
        Content
      </Modal>
    );

    const [removeButton] = wrapper.find("button");
    expect(removeButton).not.toBeNull();
    removeButton.props.onClick();

    expect(handler.mock.calls.length).toBe(1);
  });

  test("should be able to trigger the close handler where clicking outside", async () => {
    const handleCancel = jest.fn();

    // We should mock the "document.addEventListener" function
    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    // Initialize the component
    mount(
      <Modal title="My Modal" handleCancel={handleCancel}>
        Content
      </Modal>
    );

    // When the user clicks the somewhere in the page but not the modal component
    map.mousedown({ target: null });

    // handleCancel should be triggerred once
    expect(handleCancel.mock.calls.length).toBe(1);
  });
});
