import React, { useEffect, useRef } from "react";
import styled from "styled-components";

const BlackBox = styled.div`
  position: absolute;
  top: 0px;
  bottom: 0px;
  right: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.75;
  width: 100%;
  height: 100%;
`;

const ModalBox = styled.div`
  position: absolute;
  top: 40px;
  left: 0px;
  right: 0px;
  margin: auto;
  width: 500px;
  background-color: #d8d8d8;
  border: 1px solid #333;
  border-radius: 4px;
  font-size: 20px;
  box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.25);
  -webkit-box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.25);
  -moz-box-shadow: 0px 0px 10px 3px rgba(150, 150, 150, 0.25);
`;

const Header = styled.div`
  background-color: black;
  color: white;
  padding: 8px 12px;
  font-size: 16px;
  font-weight: bold;
`;

const CloseIcon = styled.button`
  float: right;
  background-color: transparent;
  color: white;
  border: none;
  font-size: 26px;
  font-weight: bold;
  cursor: pointer;
  padding: 0px;
  margin-top: -7px;
  margin-right: -10px;
  width: 30px;
`;

const Body = styled.div`
  padding: 12px;
`;

export default function Modal({ title, children, handleCancel }) {
  const modalRef = useRef();

  const handleClickOutside = (event) => {
    if (modalRef && !modalRef.current.contains(event.target)) {
      document.removeEventListener("mousedown", handleClickOutside);
      handleCancel();
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <>
      <BlackBox className="black-box" />
      <ModalBox ref={modalRef}>
        <Header>
          {title}
          <CloseIcon type="button" onClick={handleCancel}>
            x
          </CloseIcon>
        </Header>
        <Body>{children}</Body>
      </ModalBox>
    </>
  );
}
