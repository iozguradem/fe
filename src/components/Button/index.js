import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  background-color: black;
  color: white;
  font-size: 16px;
  border-radius: 51px;
  padding: 10px 30px;
  border: none;
  cursor: pointer;
  font-weight: bold;
  margin-right: 5px;
  margin-left: 5px;
`;

export default function Button({ children, defaultType = "button", ...props }) {
  return (
    <StyledButton type={defaultType} {...props}>
      {children}
    </StyledButton>
  );
}
