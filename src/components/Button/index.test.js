import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Button from "./index";

configure({ adapter: new Adapter() });

describe("<Button />", () => {
  test("should be able to render my button title", () => {
    const wrapper = shallow(<Button>My Button</Button>);
    const html = wrapper.html();
    expect(html).toContain(`<button`);
    expect(html).toContain(`My Button`);
  });

  test("should be able to render the button with default type", () => {
    const wrapper = shallow(<Button>My Button</Button>);
    expect(wrapper.html()).toContain(`type="button"`);
  });

  test("should be able to set the button type", () => {
    const wrapper = shallow(<Button defaultType="submit">My Button</Button>);
    expect(wrapper.html()).toContain(`type="submit"`);
  });
});
