import React from "react";
import styled from "styled-components";

const Container = styled.div`
  background-color: #eee;
  height: 80px;
  width: 80px;
  margin-right: 15px;
  border: 1px solid #ccc;
  border-radius: 4px;
  text-align: center;
  padding: 6px;
`;

const Point = styled.div`
  font-weight: bold;
  font-size: 30px;
  position: relative;
  top: 10px;
`;

const Text = styled.div`
  position: relative;
  top: 16px;
  font-size: 14px;
`;

export default function ListItemPoint({ value }) {
  return (
    <Container>
      <Point>{value}</Point>
      <Text>POINTS</Text>
    </Container>
  );
}
