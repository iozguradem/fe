import React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 50%;
`;

const StyledButton = styled.button`
  background-color: unset;
  border: none;
  padding: 0px;
  margin: 0px;
  color: #999;
  font-size: 13px;
  font-weight: bold;
  cursor: pointer;

  &:hover {
    color: black;
  }
`;
export default function ListItemAction({ children, onClick }) {
  return (
    <Container>
      <StyledButton type="button" onClick={onClick} data-testid="vote-button">
        {children}
      </StyledButton>
    </Container>
  );
}
