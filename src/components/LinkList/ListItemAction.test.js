import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ListItemAction from "./ListItemAction";

configure({ adapter: new Adapter() });

describe("<ListItemAction />", () => {
  test("should be able to render the button", () => {
    const wrapper = shallow(<ListItemAction>Content</ListItemAction>);
    expect(wrapper.text()).toBe("Content");
    expect(wrapper.html()).toContain("<button");
  });

  test("should be able to trigger the onClick handler", () => {
    const handler = jest.fn();
    const wrapper = mount(
      <ListItemAction onClick={handler}>Content</ListItemAction>
    );

    const [button] = wrapper.find("button");
    button.props.onClick();
    expect(handler.mock.calls.length).toBe(1);
  });
});
