import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ListItemPoint from "./ListItemPoint";

configure({ adapter: new Adapter() });

describe("<ListItemPoint />", () => {
  test("should be able to render the value", () => {
    const wrapper = shallow(<ListItemPoint value="12" />);
    expect(wrapper.text()).toBe("12POINTS");
    expect(wrapper.html()).toContain(">POINTS</div></div>");
  });
});
