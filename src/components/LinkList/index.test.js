import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import LinkLink from "./index";

configure({ adapter: new Adapter() });

const items = [
  {
    id: "hepsiburada-unique-id",
    name: "Hepsiburada",
    url: "https://hepsiburada.com",
    vote: 12,
  },
  {
    id: "google-id",
    name: "Google",
    url: "https://google.com",
    vote: 5,
  },
];

describe("<LinkLink />", () => {
  test("should be able display the empty message", () => {
    const wrapper = shallow(<LinkLink items={[]} />);
    expect(wrapper.text()).toBe("There is not any link yet");
  });

  test("should be able to show all items", () => {
    const wrapper = mount(<LinkLink items={items} />);
    expect(wrapper.text()).toContain("12POINTSHepsiburada");
    expect(wrapper.text()).toContain("5POINTSGoogle");
  });
});
