import React from "react";
import styled from "styled-components";
import ListItem from "./ListItem";

const EmptyBox = styled.div`
  margin-left: 5px;
  padding: 20px;
  border-radius: 4px;
  background-color: #fff6f0;
  color: #555;
`;

export default function LinkList({ items, handleVoteItem, handleRemove }) {
  return (
    <>
      {items.length === 0 && <EmptyBox>There is not any link yet</EmptyBox>}
      {items.map((item) => (
        <ListItem
          key={item.id}
          item={item}
          handleVoteItem={handleVoteItem}
          handleRemove={handleRemove}
        />
      ))}
    </>
  );
}
