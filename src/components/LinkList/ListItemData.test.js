import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ListItemData from "./ListItemData";

configure({ adapter: new Adapter() });

const item = {
  id: "hepsiburada-unique-id",
  name: "Hepsiburada",
  url: "https://hepsiburada.com",
  vote: 12,
};

describe("<ListItemData />", () => {
  test("should be able to render the item", () => {
    const wrapper = shallow(<ListItemData item={item} />);
    expect(wrapper.text()).toContain(item.name);
    expect(wrapper.text()).toContain(item.url);
    expect(wrapper.text()).toContain("<ListItemAction />");
  });

  test("should be able to create the url link", () => {
    const wrapper = shallow(<ListItemData item={item} />);
    expect(wrapper.html()).toContain(`href="https://hepsiburada.com"`);
    expect(wrapper.html()).toContain(`rel="noopener noreferrer"`);
    expect(wrapper.html()).toContain(`target="_blank"`);
  });

  test("should be able to create the voting buttons", () => {
    const wrapper = shallow(<ListItemData item={item} />);
    expect(wrapper.html()).toContain("Up Vote");
    expect(wrapper.html()).toContain("Down Vote");
    expect(wrapper.html()).toContain("<button");
  });

  test("should be able to trigger the voting functions", () => {
    const handleVoteItem = jest.fn();
    const wrapper = mount(
      <ListItemData item={item} handleVoteItem={handleVoteItem} />
    );

    const [upVote, downVote] = wrapper.find("button");
    upVote.props.onClick();
    downVote.props.onClick();

    // Should be triggerred twice
    expect(handleVoteItem.mock.calls.length).toBe(2);

    // Should be trigger upVote first
    expect(handleVoteItem.mock.calls[0][0]).toBe(item.id);
    expect(handleVoteItem.mock.calls[0][1]).toBe(1);

    // Should be trigger downVote secondly
    expect(handleVoteItem.mock.calls[1][0]).toBe(item.id);
    expect(handleVoteItem.mock.calls[1][1]).toBe(-1);
  });
});
