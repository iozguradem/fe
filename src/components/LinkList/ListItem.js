import React from "react";
import styled from "styled-components";
import ListItemPoint from "./ListItemPoint";
import ListItemData from "./ListItemData";

const Container = styled.div`
  margin-bottom: 5px;
  display: flex;
  justify-content: space-between;
  padding: 5px;
  border-radius: 4px;

  & button.remove {
    opacity: 0;
  }

  &:hover {
    background-color: #eee;

    & button.remove {
      opacity: 1;
    }
  }
`;

const RemoveButton = styled.button`
  margin-top: -18px;
  margin-right: -18px;
  background-color: #c90000;
  height: 24px;
  width: 24px;
  border: 3px solid white;
  border-radius: 50%;
  color: white;
  font-size: 30px;
  box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.2);
  cursor: pointer;
  opacity: inherit;
  padding: 0px;

  &:focus {
    outline: none;
  }
`;

const Minus = styled.div`
  background-color: white;
  height: 3px;
  width: 12px;
  margin-left: 3px;
  border-radius: 2px;
`;

export default function ListItem({ item, handleVoteItem, handleRemove }) {
  return (
    <>
      <Container>
        <ListItemPoint value={item.vote} />
        <ListItemData item={item} handleVoteItem={handleVoteItem} />
        <RemoveButton
          type="button"
          className="remove"
          data-testid="remove-button"
          onClick={() => handleRemove(item)}
        >
          <Minus />
        </RemoveButton>
      </Container>
    </>
  );
}
