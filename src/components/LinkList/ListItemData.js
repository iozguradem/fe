import React from "react";
import styled from "styled-components";
import ListItemAction from "./ListItemAction";

const Container = styled.div`
  flex-grow: 4;
`;

const ItemTitle = styled.div`
  font-weight: bold;
  font-size: 18px;
  padding-top: 5px;
  margin-bottom: 5px;
`;

const ItemLink = styled.a`
  color: #aaa;
  cursor: pointer;

  &:hover {
    color: black;
  }
`;

const ItemActions = styled.div`
  margin-top: 22px;
  display: flex;
`;

export default function ListItemData({ item, handleVoteItem }) {
  return (
    <Container>
      <ItemTitle>{item.name}</ItemTitle>
      <ItemLink href={item.url} target="_blank" rel="noopener noreferrer">
        ({item.url})
      </ItemLink>
      <ItemActions>
        <ListItemAction onClick={() => handleVoteItem(item.id, +1)}>
          🡩 Up Vote
        </ListItemAction>
        <ListItemAction onClick={() => handleVoteItem(item.id, -1)}>
          🡫 Down Vote
        </ListItemAction>
      </ItemActions>
    </Container>
  );
}
