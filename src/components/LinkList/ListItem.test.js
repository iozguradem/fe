import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ListItem from "./ListItem";

configure({ adapter: new Adapter() });

const item = {
  id: "hepsiburada-unique-id",
  name: "Hepsiburada",
  url: "https://hepsiburada.com",
  vote: 12,
};

describe("<ListItem />", () => {
  test("should be able to call the child components", () => {
    const wrapper = shallow(<ListItem item={item} />);
    expect(wrapper.text()).toBe("<ListItemPoint /><ListItemData />");
  });

  test("should be able to render the data", () => {
    const wrapper = mount(<ListItem item={item} />);
    expect(wrapper.text()).toContain(item.vote);
    expect(wrapper.text()).toContain(item.name);
    expect(wrapper.text()).toContain(item.url);
    expect(wrapper.text()).toContain("Up Vote");
    expect(wrapper.text()).toContain("Down Vote");
  });

  test("should be able to trigger the delete button", () => {
    const handler = jest.fn();
    const wrapper = mount(<ListItem item={item} handleRemove={handler} />);

    const [, , deleteButton] = wrapper.find("button");
    expect(deleteButton.props.className).toContain("remove");
    deleteButton.props.onClick();

    expect(handler.mock.calls.length).toBe(1);
    expect(handler.mock.calls[0][0]).toBe(item);
  });
});
