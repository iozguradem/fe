import React from "react";
import { MemoryRouter } from "react-router";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Header from "./index";

configure({ adapter: new Adapter() });

describe("<Header />", () => {
  test("should be able show the message on the header", () => {
    const wrapper = mount(
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    );
    expect(wrapper.text()).toContain("LinkVOTE Challange");
  });

  test("should be able show the logo", () => {
    const wrapper = mount(
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    );
    expect(wrapper.html()).toContain("<svg");
  });
});
