import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Logo } from "./../../Icons";

const Container = styled.div`
  border-bottom: 1px solid #aaaaaa;
  padding: 18px 0px;
  display: flex;
  justify-content: space-between;
`;

const LogoButton = styled(Link)`
  cursor: pointer;
`;

const HeaderTitle = styled.div`
  font-size: 25px;
`;

export default function Header() {
  return (
    <Container>
      <LogoButton to={{ pathname: "/" }}>
        <Logo />
      </LogoButton>
      <HeaderTitle>
        <strong>Link</strong>VOTE Challange
      </HeaderTitle>
    </Container>
  );
}
