import React from "react";
import styled from "styled-components";
import Modal from "../Modal";
import Button from "../Button";

const Body = styled.div`
  padding: 40px 0px;
  text-align: center;
`;

const ActionButtons = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 15px;
`;

export default function Question({ title, children, handleOk, handleCancel }) {
  return (
    <Modal title={title} handleCancel={handleCancel}>
      <Body>{children}</Body>
      <ActionButtons>
        <Button onClick={handleOk}>OK</Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </ActionButtons>
    </Modal>
  );
}
