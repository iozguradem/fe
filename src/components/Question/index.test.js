import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Question from "./index";

configure({ adapter: new Adapter() });

describe("<Question />", () => {
  test("should be able use the modal component internally", () => {
    const wrapper = shallow(
      <Question title="Question Title">Are you sure?</Question>
    );
    expect(wrapper.text()).toBe("<Modal />");
  });

  test("should be able show all data in the question modal", () => {
    const wrapper = mount(
      <Question title="Question Title">Are you sure?</Question>
    );
    expect(wrapper.text()).toContain("Question Title");
    expect(wrapper.text()).toContain("Are you sure?");
    expect(wrapper.text()).toContain("OK");
    expect(wrapper.text()).toContain("Cancel");
  });

  test("should be able close the question by clicking the cancel button", () => {
    const handleCancel = jest.fn();
    const wrapper = mount(
      <Question title="Question Title" handleCancel={handleCancel}>
        Are you sure?
      </Question>
    );

    const [cancelButton] = wrapper.find("button");
    cancelButton.props.onClick();

    expect(handleCancel.mock.calls.length).toBe(1);
  });

  test("should be able accept the question by clicking the OK button", () => {
    const handleOk = jest.fn();
    const wrapper = mount(
      <Question title="Question Title" handleOk={handleOk}>
        Are you sure?
      </Question>
    );

    const [, okButton] = wrapper.find("button");
    okButton.props.onClick();

    expect(handleOk.mock.calls.length).toBe(1);
  });
});
