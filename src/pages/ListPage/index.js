import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import Question from "./../../components/Question";
import SubmitSection from "./SubmitSection";
import SortingSection from "./SortingSection";
import ListSection from "./ListSection";
import PaginationSection from "./PaginationSection";
import sorter from "./sorter";
import useItems from "../../hooks/useItems";
import { useQuery } from "../../helpers/url";

export default function ListPage({ setToast }) {
  const params = useQuery();
  const history = useHistory();
  const [items, setItems] = useItems();
  const [deleteQuestion, setDeleteQuestion] = useState(null);
  const [pagination, setPagination] = useState({
    sorting: params.get("sorting") || "lastAddedDesc",
    perPage: parseInt(params.get("perPage") || 5),
    currentPage: parseInt(params.get("currentPage") || 0),
  });

  useEffect(() => {
    params.set("sorting", pagination.sorting);
    params.set("perPage", pagination.perPage);
    params.set("currentPage", pagination.currentPage);
    history.push({ search: params.toString() });
  }, []);

  useEffect(() => {
    setPagination({
      ...pagination,
      total: items.length,
      pageCount: Math.ceil(items.length / pagination.perPage),
    });
  }, [items]);

  const setSearchQuery = (field, value) => {
    params.set(field, value);
    history.push({ search: params.toString() });
  };

  useEffect(() => {
    const newState = {
      ...pagination,
      sorting: params.get("sorting") || "lastAddedDesc",
      perPage: parseInt(params.get("perPage") || 5),
      currentPage: parseInt(params.get("currentPage") || 0),
      total: items.length,
      pageCount: Math.ceil(items.length / pagination.perPage),
    };
    setPagination(newState);
  }, [params.toString()]);

  const sortedItems = sorter(pagination.sorting, items);
  const startIndex = pagination.currentPage * pagination.perPage;
  const filteredItems = sortedItems.slice(
    startIndex,
    startIndex + pagination.perPage
  );

  const handleVoteItem = (id, value) => {
    const newState = items.map((item) => {
      if (item.id === id) {
        return {
          ...item,
          vote: item.vote + value,
          lastVotedAt: new Date(),
        };
      }
      return item;
    });
    setItems(newState);
  };

  const handleDeleteConfirmation = () => {
    const { id, name } = deleteQuestion;
    setDeleteQuestion(null);
    const newState = items.filter((item) => item.id !== id);
    setItems(newState);
    setToast(`<b>${name}</b> removed.`);
  };

  return (
    <>
      <SubmitSection />
      <SortingSection
        pagination={pagination}
        handleSort={(event) => setSearchQuery("sorting", event.target.value)}
      />
      <ListSection
        items={filteredItems}
        handleVoteItem={handleVoteItem}
        handleRemove={(item) => setDeleteQuestion(item)}
      />
      <PaginationSection
        pagination={pagination}
        setPage={(value) => setSearchQuery("currentPage", value)}
      />
      {deleteQuestion && (
        <Question
          title="Remove Link"
          handleCancel={() => setDeleteQuestion(null)}
          handleOk={handleDeleteConfirmation}
        >
          <div>Dou you want to remove:</div>
          <strong>{deleteQuestion.name}</strong>
        </Question>
      )}
    </>
  );
}
