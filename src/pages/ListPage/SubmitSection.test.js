import React from "react";
import { configure, mount } from "enzyme";
import { MemoryRouter } from "react-router";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import SubmitSection from "./SubmitSection";

configure({ adapter: new Adapter() });

describe("<SubmitSection />", () => {
  test("should be able show the submit link", () => {
    const wrapper = mount(
      <MemoryRouter>
        <SubmitSection />
      </MemoryRouter>
    );
    expect(wrapper.text()).toContain("SUBMIT A LINK");
    expect(wrapper.html()).toContain("<a");
    expect(wrapper.html()).toContain(`href="/add"`);
  });
});
