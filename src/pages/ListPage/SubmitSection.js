import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Container = styled.div`
  border-bottom: 1px solid #aaaaaa;
  padding: 10px 5px;
  margin-bottom: 10px;
`;

const SubmitButton = styled(Link)`
  background-color: #f1f1f1;
  width: 100%;
  font-size: 30px;
  padding: 5px;
  color: #222;
  border: 1px solid #ddd;
  border-radius: 4px;
  display: flex;
  align-items: center;
  cursor: pointer;
  text-decoration: none;

  &:hover {
    background-color: #ddd;
  }
`;

const SubmitButtonIcon = styled.div`
  background-color: #ddd;
  width: 70px;
  max-height: 70px;
  border: 1px solid #aaa;
  border-radius: 4px;
  text-align: center;
  vertical-align: middle;
  padding: 16px 0px;
  margin-right: 15px;
`;
export default function SubmitSection() {
  return (
    <Container>
      <SubmitButton to="/add" data-testid="form-link">
        <SubmitButtonIcon>+</SubmitButtonIcon>
        <div>SUBMIT A LINK</div>
      </SubmitButton>
    </Container>
  );
}
