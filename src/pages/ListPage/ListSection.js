import React from "react";
import styled from "styled-components";
import LinkList from "../../components/LinkList";

const Container = styled.div`
  margin-bottom: 15px;
`;

export default function ListSection({ items, handleVoteItem, handleRemove }) {
  return (
    <Container>
      <LinkList
        items={items}
        handleVoteItem={handleVoteItem}
        handleRemove={handleRemove}
      />
    </Container>
  );
}
