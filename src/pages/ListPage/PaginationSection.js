import React from "react";
import styled from "styled-components";

const Container = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: center;
`;

const StyledLink = styled.button`
  background-color: white;
  margin-right: 5px;
  border: none;
  font-size: 18px;
  font-weight: bold;
  padding: 4px 10px;
  color: #111;
  cursor: pointer;

  &.left-mover {
    margin-right: 20px;
  }

  &.right-mover {
    margin-left: 20px;
  }

  &:hover {
    color: black;
  }

  &.is-active {
    border: 1px solid #aaa;
  }

  &:focus {
    outline: none;
  }

  &:disabled {
    color: #ddd;
  }
`;

const Link = ({ children, isActive = false, ...props }) => {
  return (
    <StyledLink className={isActive && "is-active"} {...props}>
      {children}
    </StyledLink>
  );
};

export default function PaginationSection({ pagination, setPage }) {
  return (
    <Container>
      <Link
        className="left-mover"
        onClick={() => setPage(pagination.currentPage - 1)}
        disabled={pagination.currentPage === 0}
      >
        ⯇
      </Link>
      {[...Array(pagination.pageCount)].map((value, index) => (
        <Link
          key={index}
          isActive={index === pagination.currentPage}
          onClick={() => setPage(index)}
        >
          {index + 1}
        </Link>
      ))}
      <Link
        className="right-mover"
        onClick={() => setPage(pagination.currentPage + 1)}
        disabled={pagination.currentPage === pagination.pageCount - 1}
      >
        ⯈
      </Link>
    </Container>
  );
}
