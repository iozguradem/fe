import React from "react";
import { configure, mount } from "enzyme";
import { MemoryRouter } from "react-router";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";
import ListPage from "./index";

configure({ adapter: new Adapter() });

const items = [
  {
    id: 1,
    name: "Hepsiburada",
    url: "https://hepsiburada.com",
    vote: 0,
  },
];
const setItems = (newState) => {
  items.splice(0, items.length);
  items.push(...newState);
};

jest.mock("./../../hooks/useItems", function () {
  return () => {
    return [items, setItems];
  };
});

describe("<ListPage />", () => {
  test("should be able show the links", () => {
    const wrapper = mount(
      <MemoryRouter>
        <ListPage />
      </MemoryRouter>
    );
    expect(wrapper.text()).toContain("SUBMIT A LINK");
    expect(wrapper.text()).toContain("Hepsiburada");
    expect(wrapper.text()).toContain("https://hepsiburada.com");
  });

  test("should be able to vote an item", async () => {
    const setToast = jest.fn();
    render(
      <MemoryRouter>
        <ListPage setToast={setToast} />
      </MemoryRouter>
    );

    const [upVoteButton, downVoteButton] = await screen.findAllByTestId(
      "vote-button"
    );
    user.click(upVoteButton);
    user.click(upVoteButton);

    expect(items.length).toBe(1);
    expect(items[0].vote).toBe(2);

    user.click(downVoteButton);
    expect(items[0].vote).toBe(1);
  });

  test("should be able to delete an item with confirmation", () => {
    const setToast = jest.fn();
    render(
      <MemoryRouter>
        <ListPage setToast={setToast} />
      </MemoryRouter>
    );

    // Try to delete
    const removeButton = screen.getByTestId("remove-button");
    expect(removeButton).not.toBeNull();
    user.click(removeButton);

    // Try to cancel
    screen.getByText("Dou you want to remove:");
    const cancelButton = screen.getByText("Cancel");
    user.click(cancelButton);
    screen.getByText("Hepsiburada");

    // Try to delete again
    user.click(removeButton);
    screen.getByText("Dou you want to remove:");
    const okButton = screen.getByText("OK");
    user.click(okButton);

    expect(setToast.mock.calls.length).toBe(1);
    expect(setToast.mock.calls[0][0]).toBe("<b>Hepsiburada</b> removed.");
  });
});
