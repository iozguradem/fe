import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import SortingSection from "./SortingSection";

configure({ adapter: new Adapter() });

const pagination = {
  sorting: "lastAddedDesc",
};

describe("<SortingSection />", () => {
  test("should be able show all selections", () => {
    const handleSort = jest.fn();
    const wrapper = mount(
      <SortingSection pagination={pagination} handleSort={handleSort} />
    );
    expect(wrapper.text()).toContain("Insert Date (Z 🡒 A)");
    expect(wrapper.text()).toContain("Insert Date (A 🡒 Z)");
    expect(wrapper.text()).toContain("Most Voted (Z 🡒 A)");
    expect(wrapper.text()).toContain("Less Voted (A 🡒 Z)");
    expect(wrapper.html()).toContain("Order by");
  });

  test("should be able set the selection", () => {
    const handleSort = jest.fn();
    const wrapper = mount(
      <SortingSection pagination={pagination} handleSort={handleSort} />
    );

    const [selectInput] = wrapper.find("select");
    expect(selectInput.props.value).toBe("lastAddedDesc");

    selectInput.props.onChange("mostVotedAsc");
    expect(handleSort.mock.calls.length).toBe(1);
    expect(handleSort.mock.calls[0][0]).toBe("mostVotedAsc");
  });
});
