import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import PaginationSection from "./PaginationSection";

configure({ adapter: new Adapter() });

const pagination = {
  currentPage: 0,
  pageCount: 5,
};

describe("<PaginationSection />", () => {
  test("should be able show page numbers", () => {
    const wrapper = mount(<PaginationSection pagination={pagination} />);
    expect(wrapper.text()).toBe("⯇12345⯈");
  });

  test("should be able use Link component", () => {
    const wrapper = shallow(<PaginationSection pagination={pagination} />);
    expect(wrapper.text()).toContain("<Link />");
  });

  test("should be able mark the left mover as disabled in the first page", () => {
    const wrapper = mount(<PaginationSection pagination={pagination} />);
    const [leftMover] = wrapper.find("button.left-mover");
    expect(leftMover.props.disabled).toBeTruthy();
  });

  test("should be able mark the right mover as not disabled in the first page", () => {
    const wrapper = mount(<PaginationSection pagination={pagination} />);
    const [rightMover] = wrapper.find("button.right-mover");
    expect(rightMover.props.disabled).toBeFalsy();
  });

  test("should be able click the page", () => {
    const setPage = jest.fn();
    const wrapper = mount(
      <PaginationSection pagination={pagination} setPage={setPage} />
    );
    const [, , secondPage] = wrapper.find("button");
    expect(secondPage.props.children).toBe(2);
    secondPage.props.onClick();

    expect(setPage.mock.calls.length).toBe(1);
    expect(setPage.mock.calls[0][0]).toBe(1);
  });

  test("should be able mark the active page", () => {
    const wrapper = mount(<PaginationSection pagination={pagination} />);
    const [, firstPage, secondPage] = wrapper.find("button");
    expect(firstPage.props.className).toContain("is-active");
    expect(secondPage.props.className).not.toContain("is-active");
  });
});
