import lastAddedDesc from "./lastAddedDesc";
import lastAddedAsc from "./lastAddedAsc";
import mostVotedAsc from "./mostVotedAsc";
import mostVotedDesc from "./mostVotedDesc";

const SORTING_ALGORITHMS = {
  lastAddedDesc,
  lastAddedAsc,
  mostVotedDesc,
  mostVotedAsc,
};

export default (sorting, items) => {
  const algorithm =
    SORTING_ALGORITHMS[sorting] || SORTING_ALGORITHMS.lastAddedDesc;
  return algorithm(items);
};
