export default (items) => {
  return [...items].sort((a, b) => {
    const priority = b.vote - a.vote;
    if (priority === 0) {
      return new Date(b.lastVotedAt) - new Date(a.lastVotedAt);
    }
    return priority;
  });
};
