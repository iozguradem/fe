export default (items) => {
  return [...items].sort((a, b) => {
    return new Date(b.createdAt) - new Date(a.createdAt);
  });
};
