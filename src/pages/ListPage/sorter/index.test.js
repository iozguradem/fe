import factory from "./index";

describe("Sorter Factory", () => {
  test("should be able sort items by date", () => {
    const algorithms = [
      "lastAddedDesc",
      "lastAddedAsc",
      "mostVotedDesc",
      "mostVotedAsc",
      "undefined-algorithm",
    ];

    const call = () => {
      algorithms.forEach((algorithm) => {
        factory(algorithm, []);
      });
    };
    expect(call).not.toThrow(Error);
  });
});
