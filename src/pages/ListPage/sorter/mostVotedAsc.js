export default (items) => {
  return [...items].sort((a, b) => {
    const priority = a.vote - b.vote;
    if (priority === 0) {
      return new Date(b.lastVotedAt) - new Date(a.lastVotedAt);
    }
    return priority;
  });
};
