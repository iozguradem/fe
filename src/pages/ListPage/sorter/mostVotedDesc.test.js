import sorter from "./mostVotedDesc";

const items = [
  { id: 7, vote: -10, lastVotedAt: new Date("2000-10-01 12:00:00") },
  { id: 6, vote: 0, lastVotedAt: new Date("2020-10-01 12:00:00") },
  { id: 5, vote: 3, lastVotedAt: new Date("2021-10-01 12:00:00") },
  { id: 4, vote: 4, lastVotedAt: new Date("2021-10-20 13:00:00") },
  { id: 3, vote: 10, lastVotedAt: new Date("2021-10-21 00:30:00") },
  { id: 2, vote: 10, lastVotedAt: new Date("2021-10-21 01:00:00") },
  { id: 1, vote: 12, lastVotedAt: new Date("2021-10-21 09:00:00") },
  { id: 0, vote: 123, lastVotedAt: new Date("2021-10-21 13:00:00") },
];

describe("mostVotedDesc sorter", () => {
  test("should be able sort items by vote count", () => {
    const result = sorter(items);
    expect(result.length).toBe(items.length);
    for (const key in result) {
      expect(result[key].id).toBe(parseInt(key));
    }
  });
});
