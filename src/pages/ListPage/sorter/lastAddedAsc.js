export default (items) => {
  return [...items].sort((a, b) => {
    return new Date(a.createdAt) - new Date(b.createdAt);
  });
};
