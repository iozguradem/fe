import sorter from "./lastAddedAsc";

const items = [
  { id: 0, createdAt: new Date("2000-10-01 12:00:00") },
  { id: 1, createdAt: new Date("2020-10-01 12:00:00") },
  { id: 2, createdAt: new Date("2021-10-01 12:00:00") },
  { id: 3, createdAt: new Date("2021-10-20 13:00:00") },
  { id: 4, createdAt: new Date("2021-10-21 01:00:00") },
  { id: 5, createdAt: new Date("2021-10-21 09:00:00") },
  { id: 6, createdAt: new Date("2021-10-21 13:00:00") },
];

describe("lastAddedAsc sorter", () => {
  test("should be able sort items by date", () => {
    const result = sorter(items);
    expect(result.length).toBe(items.length);
    for (const key in result) {
      expect(result[key].id).toBe(parseInt(key));
    }
  });
});
