import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ListSection from "./ListSection";

configure({ adapter: new Adapter() });

const items = [
  {
    id: "hepsiburada-unique-id",
    name: "Hepsiburada",
    url: "https://hepsiburada.com",
    vote: 12,
  },
  {
    id: "google-id",
    name: "Google",
    url: "https://google.com",
    vote: 5,
  },
];

describe("<ListSection />", () => {
  test("should be able use the LinkList component", () => {
    const wrapper = shallow(<ListSection items={items} />);
    expect(wrapper.text()).toBe("<LinkList />");
  });

  test("should be able show the basic data", () => {
    const wrapper = mount(<ListSection items={items} />);
    expect(wrapper.text()).toContain("Hepsiburada");
  });
});
