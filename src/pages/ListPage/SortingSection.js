import React from "react";
import styled from "styled-components";

const Container = styled.div`
  margin-bottom: 15px;
  padding: 5px;
`;

const StyledSelect = styled.select`
  background-color: #f2f2f2;
  font-size: 16px;
  padding: 4px 8px;
  border-radius: 4px;
  border-color: #aaaaaa;
  color: #333;
  min-width: 200px;

  &:focus {
    outline: none;
  }
`;

export default function SortingSection({ pagination, handleSort }) {
  return (
    <Container>
      <StyledSelect
        value={pagination.sorting}
        onChange={handleSort}
        placeholder="Order by"
      >
        <option value="lastAddedDesc">Insert Date (Z 🡒 A)</option>
        <option value="lastAddedAsc">Insert Date (A 🡒 Z)</option>
        <option value="mostVotedDesc">Most Voted (Z 🡒 A)</option>
        <option value="mostVotedAsc">Less Voted (A 🡒 Z)</option>
      </StyledSelect>
    </Container>
  );
}
