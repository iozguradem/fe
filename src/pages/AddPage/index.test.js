import React from "react";
import { configure, mount } from "enzyme";
import { MemoryRouter } from "react-router";
import { render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AddPage from "./index";

configure({ adapter: new Adapter() });

const items = [];
const setItems = (item) => {
  items.push(...item);
};

jest.mock("./../../hooks/useItems", function () {
  return () => {
    return [items, setItems];
  };
});

describe("<AddPage />", () => {
  test("should be able show the basic texts", () => {
    const wrapper = mount(
      <MemoryRouter>
        <AddPage />
      </MemoryRouter>
    );
    expect(wrapper.text()).toContain("Return the list");
    expect(wrapper.text()).toContain("Link Name:");
    expect(wrapper.text()).toContain("Link URL:");
    expect(wrapper.text()).toContain("ADD");
  });

  test("should be able save the form", async () => {
    const setToast = jest.fn();
    render(
      <MemoryRouter>
        <AddPage setToast={setToast} />
      </MemoryRouter>
    );

    const nameInput = screen.getByPlaceholderText("e.g. Alphabet");
    await user.type(nameInput, "Hepsiburada");

    const urlInput = screen.getByPlaceholderText("e.g. https://abc.xyz");
    await user.type(urlInput, "https://hepsiburada.com");

    const addButton = screen.getByText("ADD");
    user.click(addButton);

    expect(setToast.mock.calls.length).toBe(1);
    expect(setToast.mock.calls[0][0]).toBe("<b>Hepsiburada</b> added.");

    expect(items.length).toBe(1);
    expect(items[0].name).toBe("Hepsiburada");
  });
});
