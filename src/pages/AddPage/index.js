import React, { useState } from "react";
import styled from "styled-components";
import { v4 as uuidv4 } from "uuid";
import { Link, useHistory } from "react-router-dom";
import TextInput from "../../components/TextInput";
import Button from "../../components/Button";
import useItems from "../../hooks/useItems";

const ReturnLink = styled(Link)`
  display: block;
  font-size: 16px;
  font-weight: bold;
  cursor: pointer;
  margin-bottom: 30px;
  text-decoration: none;
`;

const FormTitle = styled.h1`
  margin-bottom: 40px;
`;

const ButtonContainer = styled.div`
  text-align: right;
`;

const DEFAULT_FORM_VALUES = {
  name: "",
  url: "",
};

export default function AddPage({ setToast }) {
  const history = useHistory();
  const [items, setItems] = useItems();
  const [state, setState] = useState(DEFAULT_FORM_VALUES);

  const handleInputChange = (field, value) => {
    setState({
      ...state,
      [field]: value,
    });
  };

  const addNewUrl = (event) => {
    event.preventDefault();
    setItems([
      ...items,
      {
        ...state,
        id: uuidv4(),
        vote: 0,
        createdAt: new Date(),
        lastVotedAt: new Date(),
      },
    ]);
    history.push("/");
    setToast(`<b>${state.name}</b> added.`);
  };

  return (
    <div>
      <ReturnLink to="/" data-testid="return-to-link-link">
        🡐 Return the list
      </ReturnLink>
      <FormTitle>Add New Link</FormTitle>
      <form onSubmit={addNewUrl}>
        <TextInput
          required
          label="Link Name:"
          placeholder="e.g. Alphabet"
          onChange={(event) => handleInputChange("name", event.target.value)}
        />
        <TextInput
          type="url"
          label="Link URL:"
          placeholder="e.g. https://abc.xyz"
          onChange={(event) => handleInputChange("url", event.target.value)}
          required
        />
        <ButtonContainer>
          <Button type="submit">ADD</Button>
        </ButtonContainer>
      </form>
    </div>
  );
}
