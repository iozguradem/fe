import { renderHook, act } from "@testing-library/react-hooks";
import useItems from "./useItems";

describe("useItems hook", () => {
  test("should be able to have the basic structure", () => {
    const { result } = renderHook(() => useItems());
    expect(result.current[0].length).toBe(0);
    expect(typeof result.current[1]).toBe("function");
  });

  test("should be able to update the data with the local storage", () => {
    const { result } = renderHook(() => useItems());
    const [, setState] = result.current;
    act(() => {
      setState([1, 2, 3]);
    });

    expect(result.current[0].length).toBe(3);
    expect(result.current[0][2]).toBe(3);
    expect(localStorage.getItem("URL_LIST")).toBe("[1,2,3]");
  });

  test("should be able to get data from local storage", () => {
    const { result } = renderHook(() => useItems());
    expect(result.current[0].length).toBe(3);
    expect(result.current[0][2]).toBe(3);
  });
});
