import { useState } from "react";

const STORAGE_KEY = "URL_LIST";

const loadItems = () => {
  const items = localStorage.getItem(STORAGE_KEY);
  if (items) {
    return JSON.parse(items);
  }
  return [];
};

const saveItems = (items) => {
  localStorage.setItem(STORAGE_KEY, JSON.stringify(items));
};

export default () => {
  const [state, setState] = useState(loadItems());

  const setStateWithStorage = (newState) => {
    setState(newState);
    saveItems(newState);
  };

  return [state, setStateWithStorage];
};
