# Frontend Case

This project has been developed because of the Frontend Developer interview at Hepsiburada. It has been built with React and any backend structure hasn't been used. It only works on local storage.

## Installation

In the project directory, you can run:

```
$ yarn
```

## Commands

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Folder Structure

- src
  - components: Reusable components
  - helpers: Some helper functions
  - hooks: Custom hooks
  - pages: All routing pages

## Dependencies

- [styled-components](https://styled-components.com/)

## Author

[Özgür Adem Işıklı](https://ozguradem.net)
